#ARIM Tech Test - Ricky Prahatama

Arim Tech Test is a small app build for test by ARIM.
This repository contains 2 app, one for the API backend and one for Angular front-end.
Using [Docker](https://www.docker.com/).
Using [ExpressJS](https://expressjs.com/).
Using [MongoDB](https://www.mongodb.com/)
Using [Javascript Standard](https://standardjs.com/)
Using [Angular](https://angular.io/)

##How to :
- navigate to project dir
- run "docker-compose build" and wait for build to complete
- run "docker-compose up -d" to run the env
- open [localhost:4200](http://localhost:4200) for angular app
- API can be accessed from [localhost:3001](localhost:3001)
- Mongo DB can be accessed using MongoExpress from [localhost:3002](http://localhost:3002)

##TODO :

- Setup Docker for env
- Setup API
- Setup Angular
- Use REDIS and JWT for Auth

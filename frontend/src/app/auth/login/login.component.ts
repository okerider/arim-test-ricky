import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
declare var $ :any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  modal = {
    message: null
  }

  constructor(private userService:UserService, private router:Router) { }

  ngOnInit() {
  }

  toggleModal(){
    $('.modal').modal('toggle')
  }

  submitLogin(email, password){
    this.userService.login(email, password)
      .subscribe((res) => {
        if (res.status) {
          let currentUser = JSON.stringify(res.value)
          localStorage.setItem('currentUser', currentUser)
          this.router.navigate(['/dashboard'])
        } else {
          this.modal.message = res.message
          this.toggleModal()
        }
      })
  }

}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'
declare var $ :any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  modal = {
    message: null
  }

  constructor(private userService:UserService) { }

  ngOnInit() {
  }

  toggleModal(){
    $('.modal').modal('toggle')
  }

  registerUser(email, password){
    this.userService.createUser(email, password)
      .subscribe((res) => {
        if (res.status) {
          this.modal.message = 'User created with ID : ' + res.value
          this.toggleModal()
        } else {
          this.modal.message = res.message
          this.toggleModal()
        }
      })
  }

}

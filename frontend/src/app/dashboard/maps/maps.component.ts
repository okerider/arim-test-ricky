import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var google: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var googleMaps = function () {
      $('#maps').width('100%').height('300px')
      var myLatLng = {lat: -25.363, lng: 131.044};
      var map = new google.maps.Map(document.getElementById('maps'), {
        zoom: 4,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map
      });
    }
    googleMaps()
  }

}

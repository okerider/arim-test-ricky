import { Component, OnInit } from '@angular/core';
import { MapService } from '../../services/map.service';
declare var $: any;
declare var google: any;

@Component({
  selector: 'app-dashboard-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private mapService:MapService) { }

  ngOnInit() {
    var chart1 = function () {
      var options = {
        exportEnabled: true,
        animationEnabled: true,
        data: [
        {
          type: "splineArea", //change it to line, area, bar, pie, etc
          dataPoints: [
            { y: 10 },
            { y: 6 },
            { y: 14 },
            { y: 12 },
            { y: 19 },
            { y: 14 },
            { y: 26 },
            { y: 10 },
            { y: 22 }
          ]
        }
        ]
      };
      $(".chart-1").CanvasJSChart(options);
    }
    chart1()
    var chart2 = function () {
      var options = {
        exportEnabled: true,
        animationEnabled: true,
        data: [
        {
          type: "line", //change it to line, area, bar, pie, etc
          dataPoints: [
            { y: 10 },
            { y: 6 },
            { y: 14 },
            { y: 12 },
            { y: 19 },
            { y: 14 },
            { y: 26 },
            { y: 10 },
            { y: 22 }
          ]
        }
        ]
      };
      $(".chart-2").CanvasJSChart(options);
    }
    chart2()
    var chart3 = function () {
      var options = {
        exportEnabled: true,
        animationEnabled: true,
        data: [
        {
          type: "bar", //change it to line, area, bar, pie, etc
          dataPoints: [
            { y: 10 },
            { y: 6 },
            { y: 14 },
            { y: 12 },
            { y: 19 },
            { y: 14 },
            { y: 26 },
            { y: 10 },
            { y: 22 }
          ]
        }
        ]
      };
      $(".chart-3").CanvasJSChart(options);
    }
    chart3()
    var googleMaps = function () {
      $('#maps').width('100%').height('300px')
      var myLatLng = {lat: -25.363, lng: 131.044};
      var map = new google.maps.Map(document.getElementById('maps'), {
        zoom: 4,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map
      });
    }
    googleMaps()
  }

}

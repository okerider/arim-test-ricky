import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  currentUser;

  constructor(private router:Router) { }

  ngOnInit() {
    if(!localStorage.getItem('currentUser')){
      this.router.navigate(['/login'])
    }
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
    console.log(this.currentUser.email)
  }

  logout () {
    localStorage.removeItem('currentUser')
    this.router.navigate(['/login'])
  }

}

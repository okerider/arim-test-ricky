import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(public http:Http) { }

  checkUser () {
    const currentUser = localStorage.getItem('currentUser')
    return currentUser
  }

  createUser (email, password) {
    let requestBody = {
      email: email,
      password: password
    }

    if (requestBody) {
      const header = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: header });

      return this.http
        .post('http://localhost:3001/register', requestBody, options)
        .map(res => res.json())
    }
  }

  login (email, password) {
    let requestBody = {
      email: email,
      password: password
    }

    if(requestBody) {
      return this.http
        .post('http://localhost:3001/login', requestBody)
        .map(res => res.json())
    }
  }

  logout () {
    localStorage.removeItem('currentUser')
    return true
  }
}

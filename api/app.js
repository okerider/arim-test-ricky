// --------------------
// ARIM Tech Test API
// by Ricky Prahatama (ricky.prahatama@gmail.com)
// Using Express, MongoDB, and Redis

// --------------------
// variables and const
require('dotenv').config()
const express = require('express')
var morgan = require('morgan')
var compression = require('compression')
var bodyParser = require('body-parser')

// --------------------
// mongoose
console.log('Connecting to mongo')
var mongoose = require('mongoose')
mongoose.connect('mongodb://' + process.env.MONGO_HOST + ':' + process.env.MONGO_PORT + '/' + process.env.MONGO_DB, { useMongoClient: true })
mongoose.Promise = global.Promise
var db = mongoose.connection
db.on('error', console.error.bind('DB ERROR'))
db.on('open', function () {
  console.log(`
    --------------------
    DB READY
    DB_HOST : ` + process.env.MONGO_HOST + `
    DB_PORT : ` + process.env.MONGO_PORT + `
    DB_NAME : ` + process.env.MONGO_DB + `
    --------------------
  `)
  startServer()
})

// TODO :
// connect to Redis

// --------------------
// express app
var app = express()
app.set('x-powered-by', false)

app.use(morgan('combined  '))
app.use(compression())
app.use(bodyParser.json())
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

// --------------------
// helpers
var responseHelper = function (status, value) {
  let returnVal = {
    status: status,
    message: value,
    value: value
  }

  if (status === true) {
    delete returnVal.message
  } else {
    delete returnVal.value
  }

  return returnVal
}

// --------------------
// TODO :
var userSchema = mongoose.Schema({
  email: String,
  address: [
    {
      name: String,
      coor: [String, String]
    }
  ],
  password: String
})
var User = mongoose.model('User', userSchema)

// --------------------
// Routes
app.get('/', function (req, res) {
  res.send('HENSHIN !!!')
})

// auth
app.post('/register', function (req, res) {
  var userParam = {
    email: req.body.email,
    password: req.body.password
  }

  if (userParam.email && userParam.password) {
    User.findOne({ email: userParam.email }, function (err, user) {
      if (err) {
        res.send(responseHelper(false, err))
      }
      if (!user) {
        let newUser = new User(userParam)
        newUser.save(function (err, newUser) {
          if (err) {
            res.send(responseHelper(false, err))
          }
          res.send(responseHelper(true, (newUser.id)))
        })
      } else {
        res.send(responseHelper(false, 'user exist'))
      }
    })
  } else {
    res.send(responseHelper(false, 'Please check your email and password'))
  }
})
app.post('/login', function (req, res) {
  var loginUser = {
    email: req.body.email,
    password: req.body.password
  }

  if (loginUser.email && loginUser.password) {
    User.findOne({ email: loginUser.email }, function (err, user) {
      if (err) {
        res.send(responseHelper(false, err))
      }
      console.log(user)
      if (user) {
        if (user.password === loginUser.password) {
          let currentUser = {
            id: user.id,
            email: user.email
          }
          res.send(responseHelper(true, currentUser))
        } else {
          res.send(responseHelper(false, 'Please check your password'))
        }
      } else {
        res.send(responseHelper(false, 'Please check your email'))
      }
    })
  } else {
    res.send(responseHelper(false, 'Please check your email and password'))
  }
})

// --------------------
// start the app
var startServer = function () {
  app.listen(process.env.APP_PORT, function () {
    console.log('API running on port ' + process.env.APP_PORT)
  })
}
